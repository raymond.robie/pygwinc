from gwinc.ifo.noises import *


class Mariner(nb.Budget):

    name = 'Mariner'

    noises = [
        QuantumVacuum,
        Seismic,
        SuspensionThermal,
        CoatingBrownian,
        CoatingThermoOptic,
        ITMThermoRefractive,
        ITMCarrierDensity,
        SubstrateBrownian,
        SubstrateThermoElastic,
        ExcessGas,
    ]
